resource "aws_lambda_function" "role_crud_lambda" {

  function_name = "role_crud-${lower(var.app_env)}"

  s3_bucket = "marquis-golang-scripts"
  s3_key    = aws_s3_object.s3_role_crud_zip.key

  handler          = "role_crud"
  source_code_hash = data.archive_file.role_crud_zip.output_base64sha256
  runtime          = "go1.x"
  role             = "arn:aws:iam::816303810652:role/lambda-secrets-role"

  vpc_config {
    # Every subnet should be able to reach an EFS mount target in the same Availability Zone. Cross-AZ mounts are not permitted.
    subnet_ids         = ["subnet-0935b4a1eeb5cb5dc", "subnet-0de239aeed4f8c55c", "subnet-0c6cdda4ad9bae96c"]
    security_group_ids = ["sg-01d0d393b9f24eb59"]
  }

  timeout = 120
  publish = true
}

resource "aws_cloudwatch_log_group" "role_crud_lambda" {
  name = "/aws/role_crud_lambda-${lower(var.app_env)}/${aws_lambda_function.role_crud_lambda.function_name}"

  retention_in_days = 30
}

# Assume role setup
/* resource "aws_iam_role" "lambda-secrets-role" {
  name_prefix = local.app_id

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

} */

/* # Attach role to Managed Policy
variable "iam_policy_arn" {
  description = "IAM Policy to be attached to role"
  type        = list(string)

  default = [
    "arn:aws:iam::816303810652:role/lambda-secrets-role"
  ]
} */

/* resource "aws_iam_policy_attachment" "role_attach" {
  name       = "policy-${local.app_id}"
  roles      = [aws_iam_role.lambda_exec.id]
  count      = length(var.iam_policy_arn)
  policy_arn = element(var.iam_policy_arn, count.index)
} */
