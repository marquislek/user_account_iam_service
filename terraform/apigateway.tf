resource "aws_apigatewayv2_api" "UserAccountIAM_api_gw2" {
  name          = local.app_id
  protocol_type = "HTTP"

  cors_configuration {
    allow_headers     = ["Access-Control-Allow-Headers", "Origin", "Accept", "X-Requested-With", "Content-Type", "Access-Control-Request-Method", "Access-Control-Request-Headers", "authorization", "X-Auth-Token", "Set-Cookie"]
    allow_methods     = ["OPTIONS", "POST", "GET", "PUT", "DELETE", "PATCH"]
    allow_origins     = ["https://www.marquistech.dev"]
    allow_credentials = true
  }
}

resource "aws_apigatewayv2_stage" "UserAccountIAM_api_gw2" {
  api_id = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id

  name        = local.app_id
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.user_account_iam_service_api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_cloudwatch_log_group" "user_account_iam_service_api_gw" {
  name = "/aws/user_account_iam_service_api_gw-${lower(var.app_env)}/${aws_apigatewayv2_api.UserAccountIAM_api_gw2.name}"

  retention_in_days = 30
}


// jwt_auth lambda
resource "aws_apigatewayv2_integration" "jwt_auth_lambda" {
  api_id = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id

  integration_uri        = aws_lambda_function.jwt_auth_lambda.invoke_arn
  payload_format_version = "2.0"
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
}

resource "aws_apigatewayv2_route" "jwt_auth_lambda" {
  api_id    = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id
  route_key = "ANY /jwt_auth-${lower(var.app_env)}"
  target    = "integrations/${aws_apigatewayv2_integration.jwt_auth_lambda.id}"
}

resource "aws_lambda_permission" "jwt_auth_lambda_api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.jwt_auth_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.UserAccountIAM_api_gw2.execution_arn}/*/*"
}

// user_account_reset_pw lambda
resource "aws_apigatewayv2_integration" "user_account_reset_pw_lambda" {
  api_id = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id

  integration_uri        = aws_lambda_function.user_account_reset_pw_lambda.invoke_arn
  payload_format_version = "2.0"
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
}

resource "aws_apigatewayv2_route" "user_account_reset_pw_lambda" {
  api_id    = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id
  route_key = "ANY /user_account_reset_pw-${lower(var.app_env)}"
  target    = "integrations/${aws_apigatewayv2_integration.user_account_reset_pw_lambda.id}"
}

resource "aws_lambda_permission" "user_account_reset_pw_lambda_api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.user_account_reset_pw_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.UserAccountIAM_api_gw2.execution_arn}/*/*"
}

// jwt_crud lambda
resource "aws_apigatewayv2_integration" "jwt_crud_lambda" {
  api_id = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id

  integration_uri        = aws_lambda_function.jwt_crud_lambda.invoke_arn
  payload_format_version = "2.0"
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
}

resource "aws_apigatewayv2_route" "jwt_crud_lambda" {
  api_id    = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id
  route_key = "ANY /jwt_crud-${lower(var.app_env)}"
  target    = "integrations/${aws_apigatewayv2_integration.jwt_crud_lambda.id}"
}


resource "aws_lambda_permission" "jwt_crud_lambda_api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.jwt_crud_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.UserAccountIAM_api_gw2.execution_arn}/*/*"
}

// user_account_crud lambda
resource "aws_apigatewayv2_integration" "user_account_crud_lambda" {
  api_id = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id

  integration_uri        = aws_lambda_function.user_account_crud_lambda.invoke_arn
  payload_format_version = "2.0"
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
}

resource "aws_apigatewayv2_route" "user_account_crud_lambda" {
  api_id    = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id
  route_key = "ANY /user_account_crud-${lower(var.app_env)}"
  target    = "integrations/${aws_apigatewayv2_integration.user_account_crud_lambda.id}"
}


resource "aws_lambda_permission" "user_account_crud_lambda_api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.user_account_crud_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.UserAccountIAM_api_gw2.execution_arn}/*/*"
}

// role_crud lambda
resource "aws_apigatewayv2_integration" "role_crud_lambda" {
  api_id = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id

  integration_uri        = aws_lambda_function.role_crud_lambda.invoke_arn
  payload_format_version = "2.0"
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
}

resource "aws_apigatewayv2_route" "role_crud_lambda" {
  api_id    = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id
  route_key = "ANY /role_crud-${lower(var.app_env)}"
  target    = "integrations/${aws_apigatewayv2_integration.role_crud_lambda.id}"
}


resource "aws_lambda_permission" "role_crud_lambda_api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.role_crud_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.UserAccountIAM_api_gw2.execution_arn}/*/*"
}


// user_account_role_crud lambda
resource "aws_apigatewayv2_integration" "user_account_role_crud_lambda" {
  api_id = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id

  integration_uri        = aws_lambda_function.user_account_role_crud_lambda.invoke_arn
  payload_format_version = "2.0"
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
}

resource "aws_apigatewayv2_route" "user_account_role_crud_lambda" {
  api_id    = aws_apigatewayv2_api.UserAccountIAM_api_gw2.id
  route_key = "ANY /user_account_role_crud-${lower(var.app_env)}"
  target    = "integrations/${aws_apigatewayv2_integration.user_account_role_crud_lambda.id}"
}


resource "aws_lambda_permission" "user_account_role_crud_lambda_api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.user_account_role_crud_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.UserAccountIAM_api_gw2.execution_arn}/*/*"
}
