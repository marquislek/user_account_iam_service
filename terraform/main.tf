terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0.0"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "~> 2.2.0"
    }
  }
  required_version = "~> 1.0"
  backend "s3" {
    bucket         = "marquistech-terraform-states"
    key            = "user_account_iam_service_terraform-main.tfstate"
    region         = "ap-southeast-1"
    kms_key_id     = "LockID"
    dynamodb_table = "terraform_state_lock_table"
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

variable "app_name" {
  description = "Application name"
  default     = "UserAccountIAM"
}

variable "app_env" {
  description = "Application environment tag"
  default     = "main"
}

locals {
  app_id = "${lower(var.app_name)}-main"
}

/* data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "mybinaries"
  output_path = "mybinaries.zip"
} */

// S3 bucket to put zip binaries
/* resource "aws_s3_bucket" "api_bucket" {
  bucket        = "marquis-golang-scripts"
  force_destroy = true
} */

// jwt_auth
data "archive_file" "jwt_auth_zip" {
  type = "zip"

  source_dir  = "jwt_auth_bin"
  output_path = "jwt_auth-main.zip"
}


resource "aws_s3_object" "s3_jwt_auth_zip" {
  bucket = "marquis-golang-scripts"

  key    = "jwt_auth-main.zip"
  source = data.archive_file.jwt_auth_zip.output_path

  etag = filemd5(data.archive_file.jwt_auth_zip.output_path)
}


// user_account_reset_pw
data "archive_file" "user_account_reset_pw_zip" {
  type = "zip"

  source_dir  = "user_account_reset_pw_bin"
  output_path = "user_account_reset_pw-main.zip"
}


resource "aws_s3_object" "s3_user_account_reset_pw_zip" {
  bucket = "marquis-golang-scripts"

  key    = "user_account_reset_pw-main.zip"
  source = data.archive_file.user_account_reset_pw_zip.output_path

  etag = filemd5(data.archive_file.user_account_reset_pw_zip.output_path)
}

// jwt_crud
data "archive_file" "jwt_crud_zip" {
  type = "zip"

  source_dir  = "jwt_crud_bin"
  output_path = "jwt_crud-main.zip"
}


resource "aws_s3_object" "s3_jwt_crud_zip" {
  bucket = "marquis-golang-scripts"

  key    = "jwt_crud-main.zip"
  source = data.archive_file.jwt_crud_zip.output_path

  etag = filemd5(data.archive_file.jwt_crud_zip.output_path)
}

// jwt_crud
data "archive_file" "user_account_crud_zip" {
  type = "zip"

  source_dir  = "user_account_crud_bin"
  output_path = "user_account_crud-main.zip"
}


resource "aws_s3_object" "s3_user_account_crud_zip" {
  bucket = "marquis-golang-scripts"

  key    = "user_account_crud-main.zip"
  source = data.archive_file.user_account_crud_zip.output_path

  etag = filemd5(data.archive_file.user_account_crud_zip.output_path)
}

// role_crud
data "archive_file" "role_crud_zip" {
  type = "zip"

  source_dir  = "role_crud_bin"
  output_path = "role_crud-main.zip"
}


resource "aws_s3_object" "s3_role_crud_zip" {
  bucket = "marquis-golang-scripts"

  key    = "role_crud-main.zip"
  source = data.archive_file.role_crud_zip.output_path

  etag = filemd5(data.archive_file.role_crud_zip.output_path)
}

// user_account_role_crud
data "archive_file" "user_account_role_crud_zip" {
  type = "zip"

  source_dir  = "user_account_role_crud_bin"
  output_path = "user_account_role_crud-main.zip"
}


resource "aws_s3_object" "s3_user_account_role_crud_zip" {
  bucket = "marquis-golang-scripts"

  key    = "user_account_role_crud-main.zip"
  source = data.archive_file.user_account_role_crud_zip.output_path

  etag = filemd5(data.archive_file.user_account_role_crud_zip.output_path)
}
