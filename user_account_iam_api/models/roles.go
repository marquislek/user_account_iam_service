package models

import (
	"errors"
	"user_account_iam_api/database"

	"gorm.io/gorm"
)

type User_account_role struct {
	gorm.Model
	ID              uint
	User_account_id uint
	Role_id         uint
}

type Role struct {
	gorm.Model
	ID   uint `gorm:"primaryKey"`
	Name string
}

var (
	ErrorInvalidRoleData             = "invalid Role data"
	ErrorRoleAlreadyExists           = "Role already exists"
	ErrorRoleDoesNotExist            = "Role does not exist"
	ErrorUserAccountRoleDoesNotExist = "User_account_role does not exist"
	ErrorUserAccountRoleAlreadyExist = "User_account_role already exist"
)

func init() {
	database.Connect()
	db = database.GetDB()
	db.AutoMigrate(&User_account_role{})
	db.AutoMigrate(&Role{})
}

func GetRoleById(role_id uint) (*Role, error) {
	var getRole Role
	result := db.First(&getRole, role_id)
	if result.Error != nil {
		return nil, errors.New(ErrorRoleDoesNotExist)
	}
	return &getRole, nil
}

func GetAllRoles() ([]Role, error) {
	var roles []Role
	result := db.Find(&roles)
	if result.Error != nil {
		return nil, errors.New(ErrorFailedToFetchRecord)
	}
	return roles, nil
}

func CreateRole(newRole *Role) (*Role, error) {
	_, getRoleByIdErr := GetRoleById(newRole.ID)

	if getRoleByIdErr == nil {
		return nil, errors.New(ErrorRoleAlreadyExists)
	}

	newRole.ID = 0
	result := db.Create(&newRole)
	if result.Error != nil {
		return nil, errors.New(ErrorCouldNotPutItem)
	}

	return newRole, nil
}

func DeleteRoleByID(role_id uint) (*Role, error) {
	delRole, getRoleByIdErr := GetRoleById(role_id)
	if getRoleByIdErr != nil {
		return nil, getRoleByIdErr
	}
	result := db.Delete(&delRole)
	if result.Error != nil {
		return nil, result.Error
	}
	return delRole, nil
}

func UpdateRoleById(curRole *Role) (*Role, error) {
	_, getRoleByIdErr := GetRoleById(curRole.ID)
	if getRoleByIdErr != nil {
		return nil, errors.New(ErrorUser_accountDoesNotExist)
	}

	result := db.Save(&getRoleByIdErr)
	if result.Error != nil {
		return nil, errors.New(ErrorCouldNotPutItem)
	}
	return curRole, nil
}

func GetUser_account_roleByUser_ID(userID uint) (*User_account_role, error) {
	var getUser_account_role User_account_role
	result := db.Where("user_account_id = ?", userID).First(&getUser_account_role)
	if result.Error != nil {
		return nil, errors.New(ErrorUserAccountRoleDoesNotExist)
	}
	return &getUser_account_role, nil
}

func GetAllUser_account_role() ([]User_account_role, error) {
	var user_account_roles []User_account_role
	result := db.Find(&user_account_roles)
	if result.Error != nil {
		return nil, errors.New(ErrorFailedToFetchRecord)
	}
	return user_account_roles, nil
}

func CreateUser_account_roleByUser_ID(newUser_account_role *User_account_role) (*User_account_role, error) {
	_, getNewUser_account_roleErr := GetUser_account_roleByUser_ID(newUser_account_role.User_account_id)
	if getNewUser_account_roleErr == nil {
		return nil, errors.New(ErrorUserAccountRoleAlreadyExist)
	}
	newUser_account_role.ID = 0
	result := db.Create(&newUser_account_role)
	if result.Error != nil {
		return nil, errors.New(ErrorCouldNotPutItem)
	}
	return newUser_account_role, nil
}

func DeleteUser_account_roleByUser_ID(userID uint) (*User_account_role, error) {
	delUser_account_role, getUser_account_roleByIdErr := GetUser_account_roleByUser_ID(userID)
	if getUser_account_roleByIdErr != nil {
		return nil, getUser_account_roleByIdErr
	}
	result := db.Delete(&delUser_account_role)
	if result.Error != nil {
		return nil, result.Error
	}
	return delUser_account_role, nil
}

func UpdateUser_account_roleByUser_ID(curUser_account_role *User_account_role) (*User_account_role, error) {
	_, getUser_account_roleByIdErr := GetUser_account_roleByUser_ID(curUser_account_role.User_account_id)
	if getUser_account_roleByIdErr != nil {
		return nil, getUser_account_roleByIdErr
	}
	result := db.Save(&curUser_account_role)
	if result.Error != nil {
		return nil, errors.New(ErrorCouldNotPutItem)
	}
	return curUser_account_role, nil
}
