package models

import (
	"crypto/rand"
	"errors"
	"math/big"
	"time"
	"user_account_iam_api/database"

	"golang.org/x/crypto/bcrypt"

	"gorm.io/gorm"
)

var db *gorm.DB

type User_account struct {
	gorm.Model
	ID         uint `gorm:"primaryKey"`
	Username   string
	Password   string
	Email      string
	Last_login time.Time
}

var (
	ErrorMethodNotAllowed          = "method not allowed"
	ErrorFailedToUnmarshalRecord   = "failed to unmarshal record"
	ErrorFailedToFetchRecord       = "failed to fetch record"
	ErrorInvalidUser_accountData   = "invalid User_account data"
	ErrorInvalidEmail              = "invalid email"
	ErrorCouldNotMarshalItem       = "could not marshal item"
	ErrorCouldNotDeleteItem        = "could not delete item"
	ErrorCouldNotPutItem           = "could not put item in db"
	ErrorUser_accountAlreadyExists = "User_account already exists"
	ErrorUser_accountDoesNotExist  = "User_account does not exist"
	ErrorRefreshJWTDoesNotExist    = "RefreshJWT does not exist"
	ErrorUser_accountRole          = "Issue updating user role"
)

func init() {
	database.Connect()
	db = database.GetDB()
	db.AutoMigrate(&User_account{})
}

func GetUser_accountById(user_account_id uint) (*User_account, error) {
	var getUser_account User_account
	result := db.First(&getUser_account, user_account_id)
	if result.Error != nil {
		return nil, errors.New(ErrorUser_accountDoesNotExist)
	}
	return &getUser_account, nil
}

func GetUser_accountByUsername(username string) (*User_account, error) {
	var getUser_account User_account
	result := db.Where("username = ?", username).First(&getUser_account)
	if result.Error != nil {
		return nil, errors.New(ErrorUser_accountDoesNotExist)
	}
	return &getUser_account, nil
}

func GetUser_accountByEmail(email string) (*User_account, error) {
	var getUser_account User_account
	result := db.Where("email = ?", email).First(&getUser_account)
	if result.Error != nil {
		return nil, errors.New(ErrorUser_accountDoesNotExist)
	}
	return &getUser_account, nil
}

func GetAllUser_accounts() ([]User_account, error) {
	var user_accounts []User_account
	result := db.Find(&user_accounts)
	if result.Error != nil {
		return nil, errors.New(ErrorFailedToFetchRecord)
	}
	return user_accounts, nil
}

func CreateUser_account(newUser_account *User_account) (*User_account, error) {
	_, getUser_accountByIdErr := GetUser_accountById(newUser_account.ID)
	_, getUser_accountByEmailErr := GetUser_accountByEmail(newUser_account.Email)
	_, getUsergetUser_accountByUsernameErr := GetUser_accountByUsername(newUser_account.Username)

	if getUser_accountByIdErr == nil || getUser_accountByEmailErr == nil || getUsergetUser_accountByUsernameErr == nil {
		return nil, errors.New(ErrorUser_accountAlreadyExists)
	}

	newUser_account.ID = 0

	hashedPassword, hashPasswordErr := bcrypt.GenerateFromPassword([]byte(newUser_account.Password), 10)
	if hashPasswordErr != nil {
		return nil, errors.New("password error")
	}
	newUser_account.Password = string(hashedPassword)
	newUser_account.CreatedAt = time.Now().UTC()
	newUser_account.Last_login = time.Time{}
	createResult := db.Create(&newUser_account)
	if createResult.Error != nil {
		return nil, errors.New(ErrorCouldNotPutItem)
	}
	newUser_account_role := User_account_role{
		ID:              0,
		User_account_id: newUser_account.ID,
		Role_id:         2,
	}
	_, createUser_account_roleErr := CreateUser_account_roleByUser_ID(&newUser_account_role)

	if createUser_account_roleErr != nil {
		return nil, errors.New(ErrorUser_accountRole)
	}
	return newUser_account, nil
}

func DeleteUser_accountById(user_account_id uint) (*User_account, error) {
	delUser_account, getUser_accountByIdErr := GetUser_accountById(user_account_id)
	if getUser_accountByIdErr != nil {
		return nil, getUser_accountByIdErr
	}
	result := db.Delete(&delUser_account)
	if result.Error != nil {
		return nil, result.Error
	}
	return delUser_account, nil
}

func UpdateUser_accountById(curUser_account *User_account) (*User_account, error) {
	_, getUser_accountByIdErr := GetUser_accountById(curUser_account.ID)
	if getUser_accountByIdErr != nil {
		return nil, errors.New(ErrorUser_accountDoesNotExist)
	}

	hashedPassword, hashPasswordErr := bcrypt.GenerateFromPassword([]byte(curUser_account.Password), 14)
	if hashPasswordErr != nil {
		return nil, errors.New("password error")
	}
	curUser_account.Password = string(hashedPassword)
	result := db.Save(&curUser_account)
	if result.Error != nil {
		return nil, errors.New(ErrorCouldNotPutItem)
	}
	return curUser_account, nil
}

func ResetPasswordUser_account(username_email string) (*string, error) {
	var user User_account
	record_email := db.Where("email = ?", username_email).First(&user)
	if record_email.Error != nil {
		record_username := db.Where("username = ?", username_email).First(&user)
		if record_username.Error != nil {
			return nil, record_username.Error
		}
		return nil, record_email.Error
	}

	newPassword := ""
	lowerCharSet := "abcdedfghijklmnopqrst"
	upperCharSet := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	specialCharSet := "!@#$%&*"
	numberSet := "0123456789"
	allCharSet := lowerCharSet + upperCharSet + specialCharSet + numberSet

	max := int64(len(allCharSet))
	for i := 0; i < 12; i++ {
		nBig, _ := rand.Int(rand.Reader, big.NewInt(max))

		newPassword += string(allCharSet[nBig.Int64()])
	}
	hashedPassword, hashPasswordErr := bcrypt.GenerateFromPassword([]byte(newPassword), 10)
	if hashPasswordErr != nil {
		return nil, errors.New("password error")
	}
	user.Password = string(hashedPassword)
	_, err := UpdateUser_accountById(&user)

	return &newPassword, err
}

func LoginUser_account(username_email string, password string) (*User_account, uint, error) {

	var user User_account

	record_email := db.Where("email = ?", username_email).First(&user)
	if record_email.Error != nil {
		record_username := db.Where("username = ?", username_email).First(&user)
		if record_username.Error != nil {
			return nil, 0, record_username.Error
		}
	}

	credentialError := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if credentialError != nil {
		return nil, 0, credentialError
	}

	updateUserRes := db.Model(&user).Update("last_login", time.Now().UTC())
	if updateUserRes.Error != nil {
		return &user, 0, updateUserRes.Error
	}
	userRole, userRoleErr := GetUser_account_roleByUser_ID(user.ID)
	if userRoleErr != nil {
		return &user, 0, userRoleErr
	}
	return &user, userRole.Role_id, nil
}
