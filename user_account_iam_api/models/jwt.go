package models

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"
	"user_account_iam_api/database"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/golang-jwt/jwt/v4"
	"gorm.io/gorm"
)

// aws secrets manager key-value pair
type AwsSecretKey struct {
	Key string
}

// custom struct for JWT Claims which will ultimately become the payload of the JWT
type JWTClaim struct {
	Credentials string `json:"credentials"`
	jwt.StandardClaims
}

type Refresh_JWT struct {
	gorm.Model
	ID          uint `gorm:"primaryKey"`
	User_id     uint
	Tokenstring string
	Expires     time.Time
}

var jwtSecretKey []byte

func init() {
	// get the jwt secret key
	jwtSecretKey = []byte(getJwtKey("jwt_key").Key)

	database.Connect()
	db = database.GetDB()
	db.AutoMigrate(&Refresh_JWT{})

}

// function to get jwt key from aws secrets manager
func getJwtKey(secretName string) AwsSecretKey {

	region := "ap-southeast-1"

	svc := secretsmanager.New(session.New(&aws.Config{
		Region: &region,
	}))

	input := &secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(secretName),
		VersionStage: aws.String("AWSCURRENT"),
	}

	result, err := svc.GetSecretValue(input)
	var awsSecretKey AwsSecretKey
	if err == nil {
		var secretString, decodedBinarySecret string

		if result.SecretString != nil {
			secretString = *result.SecretString
			json.Unmarshal([]byte(secretString), &awsSecretKey)
		} else {
			decodedBinarySecretBytes := make([]byte, base64.StdEncoding.DecodedLen(len(result.SecretBinary)))
			len, err := base64.StdEncoding.Decode(decodedBinarySecretBytes, result.SecretBinary)
			if err != nil {
				fmt.Println("Base64 Decode Error:", err)
			}
			decodedBinarySecret = string(decodedBinarySecretBytes[:len])
			json.Unmarshal([]byte(decodedBinarySecret), &awsSecretKey)
		}
	}
	return awsSecretKey
}

// GenerateJWT() function, which takes in credentials as parameters,
// would return the generated JWT string. Here we set a default expiration time as 1 Hour

func CreateJWTTokenString(credentials string, expirationTime time.Time) (string, error) {

	// create a new claim variable with the available data and expiration time.
	claims := &JWTClaim{
		Credentials: credentials,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// generate the token using the HS512 Signing Algorithm by passing the previously created claims.
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	// JWTp@$$word123
	tokenString, err := token.SignedString(jwtSecretKey)
	return tokenString, err
}

//  ValidateToken() function, we would take in the token string coming from the client’s HTTP request header and validate it.
func ValidateToken(signedToken string) (string, error) {

	// here we will try to parse the JWT into claims using the JWT package’s helper method “ParseWithClaims”.
	// From the parsed token, we extract the claims
	token, err := jwt.ParseWithClaims(
		signedToken,
		&JWTClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(jwtSecretKey), nil
		},
	)
	if err != nil {
		return "", err
	}
	claims, ok := token.Claims.(*JWTClaim)
	if !ok {
		err = errors.New("couldn't parse claims")
		return claims.Credentials, err
	}
	// check if the token is actually expired or not.
	if claims.ExpiresAt < time.Now().UTC().Unix() {
		err = errors.New("token expired")
		return claims.Credentials, err
	}
	return claims.Credentials, nil
}

func CreateRefreshJWT(signedToken string, userID uint, expiryTime time.Time) (*Refresh_JWT, error) {

	newRefreshJWT := &Refresh_JWT{
		User_id:     userID,
		Tokenstring: signedToken,
		Expires:     expiryTime,
	}
	createResult := db.Create(&newRefreshJWT)

	return newRefreshJWT, createResult.Error

}

func GetRefreshJWTByUserIDToken(signedToken string, userID uint) (*Refresh_JWT, error) {
	newRefreshJWT := new(Refresh_JWT)
	result := db.Where("user_id = ? AND tokenstring = ?", userID, signedToken).Limit(1).Find(&newRefreshJWT)

	return newRefreshJWT, result.Error

}

func GetRefreshJWTByID(refreshJWTID uint) (*Refresh_JWT, error) {
	getRefreshJWT := new(Refresh_JWT)
	result := db.First(&getRefreshJWT, refreshJWTID)

	if result.Error != nil {
		var s string = strconv.FormatUint(uint64(refreshJWTID), 10)
		return nil, errors.New(ErrorRefreshJWTDoesNotExist + " " + s)
	}
	return getRefreshJWT, nil

}

func DeleteRefreshJWT(refreshJWTID uint) (*Refresh_JWT, error) {
	delRefreshJWT, getRefreshJWTErr := GetRefreshJWTByID(refreshJWTID)

	if getRefreshJWTErr != nil {
		return nil, getRefreshJWTErr
	}

	result := db.Delete(&delRefreshJWT)
	if result.Error != nil {
		return nil, result.Error
	}
	return delRefreshJWT, nil

}

func DeleteExpiredJWTByUserID(userID uint) (string, error) {
	result := db.Where("user_id = ? and expires < ?", userID, time.Now().UTC()).Delete(&Refresh_JWT{})
	if result.Error != nil {
		return "delete expired jwt error", result.Error
	}
	return "successfully deleted", nil
}
