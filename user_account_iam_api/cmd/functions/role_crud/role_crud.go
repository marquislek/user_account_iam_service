package main

import (
	"user_account_iam_api/handlers"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func LambdaHandler(req events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	// return apiResponse(http.StatusAccepted, req)
	switch req.RequestContext.HTTP.Method {
	case "GET":
		return handlers.GetRole(req)
	case "POST":
		return handlers.CreateRole(req)
	case "PUT":
		return handlers.UpdateRole(req)
	case "DELETE":
		return handlers.DeleteRole(req)
	case "OPTIONS":
		return handlers.OptionsMethod()
	default:
		return handlers.UnhandledMethod()
	}
}

func main() {

	lambda.Start(LambdaHandler)

}
