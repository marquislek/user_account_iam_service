package main

import (
	"user_account_iam_api/handlers"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func LambdaHandler(req events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	switch req.RequestContext.HTTP.Method {
	case "GET":
		return handlers.RefreshAccessToken(req)
	case "POST":
		return handlers.LoginUser_account(req)
	case "DELETE":
		return handlers.LogoutUser_account(req)
	case "OPTIONS":
		return handlers.OptionsMethod()
	default:
		return handlers.UnhandledMethod()
	}
}

func main() {
	lambda.Start(LambdaHandler)

}
