package main

import (
	"user_account_iam_api/handlers"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func LambdaHandler(req events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	return handlers.ResetPasswordUser_account(req)
}

func main() {

	lambda.Start(LambdaHandler)

}
