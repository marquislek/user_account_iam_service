package main

import (
	"user_account_iam_api/handlers"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func LambdaHandler(req events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	switch req.RequestContext.HTTP.Method {
	case "OPTIONS":
		return handlers.OptionsMethod()
	default:
		return handlers.GetUserByAuthHeader(req)
	}
}

func main() {

	lambda.Start(LambdaHandler)

}
