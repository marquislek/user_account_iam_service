package handlers

import (
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
)

func apiResponse(status int, body interface{}, cookies []string) (*events.APIGatewayV2HTTPResponse, error) {
	resp := events.APIGatewayV2HTTPResponse{Headers: map[string]string{
		"Content-Type":                     "application/json",
		"Access-Control-Allow-Headers":     "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization, X-Auth-Token, Set-Cookie",
		"Access-Control-Allow-Origin":      "https://marquis-blog-next.vercel.app",
		"Access-Control-Allow-Methods":     "OPTIONS,POST,GET,PUT,DELETE,PATCH",
		"Access-Control-Allow-Credentials": "true",
		"Access-Control-Expose-Headers":    "Set-Cookie",
	}}

	resp.Cookies = cookies
	resp.StatusCode = status

	stringBody, _ := json.Marshal(body)
	resp.Body = string(stringBody)
	return &resp, nil
}
