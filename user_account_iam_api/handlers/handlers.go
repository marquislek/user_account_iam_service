package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"
	"user_account_iam_api/models"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
)

var ErrorMethodNotAllowed = "method not allowed"

type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty"`
}

type LoginCredentials struct {
	Username_email string `json:"username_email"`
	Password       string `json:"password"`
}

type Username_emailBody struct {
	Username_email string `json:"username_email"`
}

type TokenBody struct {
	JWT     string
	Expires time.Time
}

type AccessCredentials struct {
	Username string
	Email    string
	RoleID   uint
}

type RefreshCredentials struct {
	User_id     uint
	RefreshTime time.Time
}

// add auth check to ensure only admin can create account
func CreateUser_account(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {

	newUser_account := new(models.User_account)

	compareReqBodyNewUser_account := json.NewDecoder(strings.NewReader(request.Body))
	compareReqBodyNewUser_account.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyNewUser_account.Decode(&newUser_account)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(keyMismatchErr.Error()),
		}, nil)
	}

	if err := json.Unmarshal([]byte(request.Body), &newUser_account); err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("error invalid user account data"),
		}, nil)
	}
	result, err := models.CreateUser_account(newUser_account)
	if err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		}, nil)
	}
	return apiResponse(http.StatusOK, result, nil)

}

// admin can get any account but the requester can only get their own account information
func GetUser_account(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	reqParams := request.QueryStringParameters
	if val, ok := reqParams["User_account_ID"]; ok {
		User_accountID, err := strconv.Atoi(val)
		if err != nil {
			return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("User_account_ID not an integer")}, nil)
		}
		result, err := models.GetUser_accountById(uint(User_accountID))
		if err != nil {
			return apiResponse(http.StatusBadRequest,
				ErrorBody{aws.String(err.Error())},
				nil)
		}
		return apiResponse(http.StatusOK, result, nil)
	}
	result, err := models.GetAllUser_accounts()
	if err != nil {
		return apiResponse(http.StatusBadRequest,
			ErrorBody{aws.String(err.Error())},
			nil)
	}
	return apiResponse(http.StatusOK, result, nil)
}

// admin can update any account but the requester can only update their own account
func UpdateUser_account(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	newUser_account := new(models.User_account)

	compareReqBodyNewUser_account := json.NewDecoder(strings.NewReader(request.Body))
	compareReqBodyNewUser_account.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyNewUser_account.Decode(&newUser_account)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(keyMismatchErr.Error()),
		}, nil)
	}

	if err := json.Unmarshal([]byte(request.Body), &newUser_account); err != nil {
		// return nil, errors.New(ErrorInvalidUser_accountData)
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("error invalid user_account data"),
		}, nil)
	}

	result, err := models.UpdateUser_accountById(newUser_account)
	if err != nil {
		return apiResponse(http.StatusBadRequest,
			ErrorBody{aws.String(err.Error())},
			nil)
	}
	return apiResponse(http.StatusCreated, result, nil)
}

// admin can delete any account but the requester can only delete their own account
func DeleteUser_account(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	reqParams := request.QueryStringParameters
	if val, ok := reqParams["User_account_ID"]; ok {
		User_accountID, err := strconv.Atoi(val)
		if err != nil {
			return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("User_account_ID not an integer")}, nil)
		}
		result, err := models.DeleteUser_accountById(uint(User_accountID))
		if err != nil {
			return apiResponse(http.StatusBadRequest,
				ErrorBody{aws.String(err.Error())},
				nil)
		}
		return apiResponse(http.StatusOK, result, nil)
	}
	return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("User_account_ID not found")}, nil)
}

// admin can reset any account password but the requester can only reset their own account password
func ResetPasswordUser_account(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	username_emailBody := new(Username_emailBody)

	compareReqBodyUsername_emailBody := json.NewDecoder(strings.NewReader(request.Body))
	compareReqBodyUsername_emailBody.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyUsername_emailBody.Decode(&username_emailBody)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(keyMismatchErr.Error()),
		},
			nil)
	}

	if err := json.Unmarshal([]byte(request.Body), &username_emailBody); err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("error invalid username or email"),
		},
			nil)
	}
	newPassword, err := models.ResetPasswordUser_account(username_emailBody.Username_email)
	if err != nil {
		apiResponse(http.StatusBadRequest, ErrorBody{aws.String(err.Error())}, nil)
	}
	return apiResponse(http.StatusAccepted, newPassword, nil)
}

func LoginUser_account(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	loginCredentials := new(LoginCredentials)

	compareReqBodyLoginCreds := json.NewDecoder(strings.NewReader(request.Body))
	compareReqBodyLoginCreds.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyLoginCreds.Decode(&loginCredentials)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(keyMismatchErr.Error()),
		},
			nil)
	}

	if err := json.Unmarshal([]byte(request.Body), &loginCredentials); err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("error invalid login credentials please provide username or email and password"),
		},
			nil)

	}

	user, roleID, loginErr := models.LoginUser_account(loginCredentials.Username_email, loginCredentials.Password)
	if user == nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{aws.String(loginErr.Error())}, nil)
	}

	// get refresh token

	// 1 create credentials
	refreshExpirationTime := time.Now().Add(7 * 24 * time.Hour)
	refreshCredentials := &RefreshCredentials{
		User_id:     user.ID,
		RefreshTime: time.Now().UTC(),
	}

	// 2 create the refresh token cookie to be set in the response set-cookie header
	refreshTokenCookie, refreshTokenCookieErr := createRefreshJWTCookieString(*refreshCredentials, refreshExpirationTime)
	if refreshTokenCookieErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(refreshTokenCookieErr.Error()),
		}, nil)
	}
	accessToken, accessTokenErr := createAccessToken(user.Username, user.Email, roleID)
	if accessTokenErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(accessTokenErr.Error()),
		}, nil)
	}
	return apiResponse(http.StatusAccepted, accessToken, []string{refreshTokenCookie})

}

func LogoutUser_account(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {

	// get refresh token from cookie
	getRefreshJWTTokenString, _ := getRefreshTokenStringFromRequestCookies(request)

	// validate refresh token
	validRefreshJWT, _ := models.ValidateToken(getRefreshJWTTokenString)

	// unmarshal refresh jwt
	currentRefreshCreds := new(RefreshCredentials)

	compareReqBodyReqRefreshCreds := json.NewDecoder(strings.NewReader(validRefreshJWT))
	compareReqBodyReqRefreshCreds.DisallowUnknownFields()
	compareReqBodyReqRefreshCreds.Decode(&currentRefreshCreds)

	// delete specific token from db
	getDelRefreshJWT, _ := models.GetRefreshJWTByUserIDToken(getRefreshJWTTokenString, currentRefreshCreds.User_id)

	models.DeleteRefreshJWT(getDelRefreshJWT.ID)

	// clear cookie
	expiryTime := time.Now().Add(1 * time.Minute)
	emptyRefreshJWTCookie := `marquisRefreshToken=` + "" + `;` + `Expires=` + expiryTime.UTC().String() + `;` + "Secure;" + "SameSite=None;" + "HttpOnly;"
	return apiResponse(http.StatusAccepted, nil, []string{emptyRefreshJWTCookie})
}

// this function is used to get a new access token via the refresh token in the cookie
func RefreshAccessToken(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	emptyRefreshJWTCookie := `marquisRefreshToken=` + "" + `;` + `Expires=` + "" + `;` + "Secure;" + "SameSite=None;" + "HttpOnly;"
	// validate token and stuff
	// get refresh token from cookie
	getRefreshJWTTokenString, getRefreshJWTTokenStringErr := getRefreshTokenStringFromRequestCookies(request)

	if getRefreshJWTTokenStringErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String(`RefreshAccessToken get refresh token string error: ` + getRefreshJWTTokenStringErr.Error()),
		}, nil)
	}

	// validate refresh token
	validRefreshJWTCreds, validRefreshJWTCredsErr := models.ValidateToken(getRefreshJWTTokenString)
	if validRefreshJWTCredsErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String(`RefreshAccessToken invalid refresh jwt: ` + validRefreshJWTCredsErr.Error()),
		}, nil)
	}

	currentRefreshCreds := new(RefreshCredentials)

	compareReqBodyReqRefreshCreds := json.NewDecoder(strings.NewReader(validRefreshJWTCreds))
	compareReqBodyReqRefreshCreds.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyReqRefreshCreds.Decode(&currentRefreshCreds)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String(`RefreshAccessToken Comparing request body and refresh credentials: ` + keyMismatchErr.Error()),
		}, []string{emptyRefreshJWTCookie})
	}

	if err := json.Unmarshal([]byte(validRefreshJWTCreds), &currentRefreshCreds); err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("RefreshAccessToken error invalid refresh credential data"),
		}, []string{emptyRefreshJWTCookie})
	}

	// validate refresh token from db
	dbRefreshJWT, getDBRefreshJWTErr := models.GetRefreshJWTByUserIDToken(getRefreshJWTTokenString, currentRefreshCreds.User_id)
	if getDBRefreshJWTErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String("RefreshAccessToken " + getRefreshJWTTokenString + " not found: " + getDBRefreshJWTErr.Error()),
		}, []string{emptyRefreshJWTCookie})
	}

	userRole, userRoleErr := models.GetUser_account_roleByUser_ID(dbRefreshJWT.User_id)
	if userRoleErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String("RefreshAccessToken get user role: " + userRoleErr.Error()),
		}, []string{emptyRefreshJWTCookie})
	}

	// create new access token
	getUser, getUserErr := models.GetUser_accountById(currentRefreshCreds.User_id)
	if getUserErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String("RefreshAccessToken get user account " + string(rune(currentRefreshCreds.User_id)) + " by id: " + getUserErr.Error()),
		}, []string{emptyRefreshJWTCookie})
	}

	newAccessToken, newAccessTokenErr := createAccessToken(getUser.Username, getUser.Email, userRole.Role_id)
	if newAccessTokenErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String("RefreshAccessToken create new access token: " + newAccessTokenErr.Error()),
		}, []string{emptyRefreshJWTCookie})
	}

	// new refresh token cookie string
	refreshExpirationTime := time.Now().Add(7 * 24 * time.Hour)
	refreshCredentials := &RefreshCredentials{
		User_id:     getUser.ID,
		RefreshTime: time.Now().UTC(),
	}
	refreshCookeString, refreshCookieStringErr := createRefreshJWTCookieString(*refreshCredentials, refreshExpirationTime)
	if refreshCookieStringErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String("RefreshAccessToken create refresh cookie string: " + refreshCookieStringErr.Error()),
		}, []string{emptyRefreshJWTCookie})
	}

	// delete previous refresh token from db
	_, delRefreshJWTErr := models.DeleteRefreshJWT(dbRefreshJWT.ID)
	if delRefreshJWTErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String("RefreshAccessToken delete refresh JWT from db: " + fmt.Sprintf("%v", dbRefreshJWT) + " " + delRefreshJWTErr.Error()),
		}, []string{emptyRefreshJWTCookie})
	}

	return apiResponse(http.StatusAccepted, newAccessToken, []string{refreshCookeString})
}

func GetUserByAuthHeader(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	// check auth header
	authHeader, authOk := request.Headers["authorization"]
	if !authOk {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String(`No authorization header found`),
		}, nil)
	}

	if strings.HasPrefix(authHeader, `Bearer `) == false {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String("Bearer not found in authorization header: " + authHeader),
		}, nil)
	}
	reqAccessTokenString := strings.Split(authHeader, ` `)[1]
	user, userErr := getUserByAccessJWT(reqAccessTokenString)
	if userErr != nil {
		return apiResponse(http.StatusUnauthorized, ErrorBody{
			aws.String("Get user by access jwt error: " + userErr.Error()),
		}, nil)
	}

	// check request option
	reqOption, reqOptionOk := request.QueryStringParameters["option"]
	if !reqOptionOk {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(`No request option found`),
		}, nil)
	}

	if reqOption == "account" {
		return apiResponse(http.StatusOK, user, request.Cookies)
	}

	if reqOption == "role" {
		userAccountRole, userRoleErr := models.GetUser_account_roleByUser_ID(user.ID)
		if userRoleErr != nil {
			return apiResponse(
				http.StatusBadRequest, ErrorBody{
					aws.String("Get user account role by user id error: " + userRoleErr.Error()),
				}, nil)
		}

		return apiResponse(http.StatusOK, userAccountRole, request.Cookies)
	}

	return apiResponse(http.StatusBadRequest, ErrorBody{
		aws.String(`No valid request option found`),
	}, nil)
}

func CreateRole(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	newRole := new(models.Role)

	compareReqBodyNewRole := json.NewDecoder(strings.NewReader(request.Body))
	compareReqBodyNewRole.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyNewRole.Decode(&newRole)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(keyMismatchErr.Error()),
		}, nil)
	}

	if err := json.Unmarshal([]byte(request.Body), &newRole); err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("error invalid role data"),
		}, nil)
	}
	result, err := models.CreateRole(newRole)
	if err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		}, nil)
	}
	return apiResponse(http.StatusOK, result, nil)
}

func UpdateRole(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	newRole := new(models.Role)

	compareReqBodyNewRole := json.NewDecoder(strings.NewReader(request.Body))
	compareReqBodyNewRole.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyNewRole.Decode(&newRole)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(keyMismatchErr.Error()),
		}, nil)
	}

	if err := json.Unmarshal([]byte(request.Body), &newRole); err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("error invalid role data"),
		}, nil)
	}
	result, err := models.UpdateRoleById(newRole)
	if err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		}, nil)
	}
	return apiResponse(http.StatusOK, result, nil)
}

func DeleteRole(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	reqParams := request.QueryStringParameters
	if val, ok := reqParams["RoleID"]; ok {
		RoleID, err := strconv.Atoi(val)
		if err != nil {
			return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("RoleID not an integer")}, nil)
		}
		result, err := models.DeleteRoleByID(uint(RoleID))
		if err != nil {
			return apiResponse(http.StatusBadRequest,
				ErrorBody{aws.String(err.Error())},
				nil)
		}
		return apiResponse(http.StatusOK, result, nil)
	}
	return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("RoleID not found")}, nil)
}

func GetRole(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	reqParams := request.QueryStringParameters
	if val, ok := reqParams["RoleID"]; ok {
		RoleID, err := strconv.Atoi(val)
		if err != nil {
			return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("RoleID not an integer")}, nil)
		}
		result, err := models.GetRoleById(uint(RoleID))
		if err != nil {
			return apiResponse(http.StatusBadRequest,
				ErrorBody{aws.String(err.Error())},
				nil)
		}
		return apiResponse(http.StatusOK, result, nil)
	}
	result, err := models.GetAllRoles()
	if err != nil {
		return apiResponse(http.StatusBadRequest,
			ErrorBody{aws.String(err.Error())},
			nil)
	}
	return apiResponse(http.StatusOK, result, nil)
}

func CreateUserAccountRole(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	newUser_account_role := new(models.User_account_role)

	compareReqBodyNewUser_account_role := json.NewDecoder(strings.NewReader(request.Body))
	compareReqBodyNewUser_account_role.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyNewUser_account_role.Decode(&newUser_account_role)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(keyMismatchErr.Error()),
		}, nil)
	}

	if err := json.Unmarshal([]byte(request.Body), &newUser_account_role); err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("error invalid role data"),
		}, nil)
	}
	result, err := models.CreateUser_account_roleByUser_ID(newUser_account_role)
	if err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		}, nil)
	}
	return apiResponse(http.StatusOK, result, nil)
}

func UpdateUserAccountRole(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	newUser_account_role := new(models.User_account_role)

	compareReqBodyNewUser_account_role := json.NewDecoder(strings.NewReader(request.Body))
	compareReqBodyNewUser_account_role.DisallowUnknownFields()
	keyMismatchErr := compareReqBodyNewUser_account_role.Decode(&newUser_account_role)
	if keyMismatchErr != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(keyMismatchErr.Error()),
		}, nil)
	}

	if err := json.Unmarshal([]byte(request.Body), &newUser_account_role); err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String("error invalid user account role data"),
		}, nil)
	}
	result, err := models.UpdateUser_account_roleByUser_ID(newUser_account_role)
	if err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		}, nil)
	}
	return apiResponse(http.StatusOK, result, nil)
}

func DeleteUserAccountRole(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	reqParams := request.QueryStringParameters
	if val, ok := reqParams["User_account_ID"]; ok {
		User_account_ID, err := strconv.Atoi(val)
		if err != nil {
			return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("User_account_role_ID not an integer")}, nil)
		}
		result, err := models.DeleteUser_account_roleByUser_ID(uint(User_account_ID))
		if err != nil {
			return apiResponse(http.StatusBadRequest,
				ErrorBody{aws.String(err.Error())},
				nil)
		}
		return apiResponse(http.StatusOK, result, nil)
	}
	return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("User_account_ID not found")}, nil)
}

func GetUserAccountRole(request events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
	reqParams := request.QueryStringParameters
	if val, ok := reqParams["User_account_ID"]; ok {
		User_account_ID, err := strconv.Atoi(val)
		if err != nil {
			return apiResponse(http.StatusBadRequest, ErrorBody{aws.String("User_account_ID not an integer")}, nil)
		}
		result, err := models.GetUser_account_roleByUser_ID(uint(User_account_ID))
		if err != nil {
			return apiResponse(http.StatusBadRequest,
				ErrorBody{aws.String(err.Error())},
				nil)
		}
		return apiResponse(http.StatusOK, result, nil)
	}
	result, err := models.GetAllUser_account_role()
	if err != nil {
		return apiResponse(http.StatusBadRequest,
			ErrorBody{aws.String(err.Error())},
			nil)
	}
	return apiResponse(http.StatusOK, result, nil)
}

func UnhandledMethod() (*events.APIGatewayV2HTTPResponse, error) {
	return apiResponse(http.StatusMethodNotAllowed, ErrorMethodNotAllowed, nil)
}

func OptionsMethod() (*events.APIGatewayV2HTTPResponse, error) {
	cookies := []string{"SameSite=None;" + "Secure;" + "Path=/;"}
	response := map[string]interface{}{
		"message": "Options are allowed",
	}
	return apiResponse(http.StatusOK, response, cookies)
}

func getUserByAccessJWT(accessJWT string) (*models.User_account, error) {

	getUser := new(models.User_account)
	// validate and unmarshal access token
	reqAccessJWTCreds, validateReqAccessTokenStringErr := models.ValidateToken(accessJWT)
	if validateReqAccessTokenStringErr != nil {
		return getUser, validateReqAccessTokenStringErr
	}
	reqAccessCreds := new(AccessCredentials)

	compareReqAccessJWTCredsAccessCreds := json.NewDecoder(strings.NewReader(reqAccessJWTCreds))
	compareReqAccessJWTCredsAccessCreds.DisallowUnknownFields()
	accessJWTCredsMismatchErr := compareReqAccessJWTCredsAccessCreds.Decode(&reqAccessCreds)
	if accessJWTCredsMismatchErr != nil {
		return getUser, accessJWTCredsMismatchErr
	}

	if err := json.Unmarshal([]byte(reqAccessJWTCreds), &reqAccessCreds); err != nil {
		return getUser, err
	}

	// compare access token and refresh token
	accessUser, getUserByUsernameErr := models.GetUser_accountByUsername(reqAccessCreds.Username)
	if getUserByUsernameErr != nil {
		return getUser, getUserByUsernameErr
	}

	return accessUser, nil
}

func createRefreshJWTCookieString(refreshCredentials RefreshCredentials, refreshExpirationTime time.Time) (string, error) {
	// cleanup unused expired refresh tokens
	_, cleanupErr := models.DeleteExpiredJWTByUserID(refreshCredentials.User_id)
	if cleanupErr != nil {
		return "", cleanupErr
	}

	refreshCredentialsByte, refreshCredentialsMarshalErr := json.Marshal(refreshCredentials)
	if refreshCredentialsMarshalErr != nil {
		return "refresh credentials marshal eror", refreshCredentialsMarshalErr
	}

	refreshTokenString, refreshTokenAuthErr := models.CreateJWTTokenString(string(refreshCredentialsByte), refreshExpirationTime.UTC())
	if refreshTokenAuthErr != nil {
		return "refresh token string error", refreshTokenAuthErr
	}

	_, createRefreshJWTErr := models.CreateRefreshJWT(refreshTokenString, refreshCredentials.User_id, refreshExpirationTime.UTC())
	if createRefreshJWTErr != nil {
		return "create refresh jwt in db error", createRefreshJWTErr
	}

	cookieString := `marquisRefreshToken=` + refreshTokenString + `;` + `Expires=` + refreshExpirationTime.UTC().String() + `;` + "Secure;" + "SameSite=None;" + "HttpOnly;"
	return cookieString, nil
}

func getRefreshTokenStringFromRequestCookies(request events.APIGatewayV2HTTPRequest) (string, error) {
	// check request cookie for refresh token
	refreshJWTTokenString := ""
	found := false
	for _, cookie := range request.Cookies {
		s := strings.Split(cookie, "=")
		if s[0] == "marquisRefreshToken" {
			found = true
			refreshJWTTokenString = s[1]
		}
	}
	if !found {
		return "refresh token string not found", errors.New("refresh token string not found in cookies as marquisRefreshToken")
	}
	return refreshJWTTokenString, nil
}

func createAccessToken(username string, email string, roleID uint) (*TokenBody, error) {
	// get access credentials
	accessExpirationTime := time.Now().Add(10 * time.Minute)
	accessCredentials := &AccessCredentials{
		Username: username,
		Email:    email,
		RoleID:   roleID,
	}

	accessCredentialsByte, accessCredentialsMarshalErr := json.Marshal(accessCredentials)
	if accessCredentialsMarshalErr != nil {
		return new(TokenBody), accessCredentialsMarshalErr
	}
	accessTokenString, accessTokenAuthErr := models.CreateJWTTokenString(string(accessCredentialsByte), accessExpirationTime.UTC())
	if accessTokenAuthErr != nil {
		return new(TokenBody), accessTokenAuthErr
	}
	accessToken := TokenBody{
		JWT:     accessTokenString,
		Expires: accessExpirationTime.UTC(),
	}
	return &accessToken, nil
}
