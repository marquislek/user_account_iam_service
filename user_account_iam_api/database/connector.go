package database

import (
	"encoding/base64"
	"encoding/json"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

type DatabaseAuth struct {
	Username             string
	Password             string
	Engine               string
	Host                 string
	Port                 int
	DbInstanceIdentifier string
}

func getDatabaseAuth(secretName string) DatabaseAuth {

	region := "ap-southeast-1"

	svc := secretsmanager.New(session.New(&aws.Config{
		Region: &region,
	}))

	input := &secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(secretName),
		VersionStage: aws.String("AWSCURRENT"),
	}

	result, err := svc.GetSecretValue(input)
	var databaseAuth = DatabaseAuth{}

	if err == nil {
		var secretString, decodedBinarySecret string

		if result.SecretString != nil {
			secretString = *result.SecretString
			json.Unmarshal([]byte(secretString), &databaseAuth)
		} else {
			decodedBinarySecretBytes := make([]byte, base64.StdEncoding.DecodedLen(len(result.SecretBinary)))
			len, err := base64.StdEncoding.Decode(decodedBinarySecretBytes, result.SecretBinary)
			if err != nil {
				fmt.Println("Base64 Decode Error:", err)
			}
			decodedBinarySecret = string(decodedBinarySecretBytes[:len])
			json.Unmarshal([]byte(decodedBinarySecret), &databaseAuth)
		}
	}
	return databaseAuth
}

func Connect() {
	databaseAuth := getDatabaseAuth("mygodb_conn")

	dbName := "mygodb"
	dbUsername := databaseAuth.Username
	dbHost := databaseAuth.Host
	dbPassword := databaseAuth.Password
	// region := "ap-southeast-1"

	dsn := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, dbUsername, dbName, dbPassword)
	d, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db = d
}

func GetDB() *gorm.DB {
	return db
}
